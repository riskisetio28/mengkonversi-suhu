package com.company;
import java.util.Scanner;
class Celcius {
    double toFahrenheit(){
        //°F = °C × 1,8 + 32
        return (KonvSuhuOOP.SuhuAwal*1.8+32);
    }
    double toReamur(){
        //°Ré = °C × 0,8
        return (KonvSuhuOOP.SuhuAwal*0.8);
    }
    double toKelvin(){
        //K = °C + 273,15
        return (KonvSuhuOOP.SuhuAwal+273.15);
    }
}
class Fahrenheit {
    double toReamur(){
        //°Ré = (°F − 32) / 2,25
        return ((KonvSuhuOOP.SuhuAwal-32)/2.25);
    }
    double toKelvin(){
        //K = (°F + 459,67) / 1,8
        return ((KonvSuhuOOP.SuhuAwal+459.67)/ 1.8);
    }
    double toCelcius(){
        //°C = (°F − 32) / 1,8
        return ((KonvSuhuOOP.SuhuAwal-32)/1.8);
    }
}
class Kelvin {
    double toFahrenheit(){
        //°F = K × 1,8 − 459,67
        return (KonvSuhuOOP.SuhuAwal*1.8-459.67);
    }
    double toReamur(){
        //°Ré = (K − 273,15) × 0,8
        return ((KonvSuhuOOP.SuhuAwal-273.15)*0.8);
    }
    double toCelcius(){
        //°C = K − 273,15
        return (KonvSuhuOOP.SuhuAwal-273.15);
    }
}
class Reamur {

    double toFahrenheit(){
        //°F = °Ré × 2,25 + 32
        return (KonvSuhuOOP.SuhuAwal*2.25+32);
    }
    double toKelvin(){
        //K = °Ré / 0,8 + 273,15
        return (KonvSuhuOOP.SuhuAwal/0.8+237.15);
    }
    double toCelcius(){
        //°C = °Ré / 0,8
        return (KonvSuhuOOP.SuhuAwal/0.8);
    }
}
class KonvSuhuOOP {
    static double SuhuAwal;
    public static void main(String[] args) {

        String k ="Program Konversi Suhu[y/n]:";
        Celcius fromCelcius = new Celcius();
        Fahrenheit fromFahrenheit = new Fahrenheit();
        Reamur fromReamur = new Reamur();
        Kelvin fromKelvin = new Kelvin();
        double Fahrenheit, Kelvin,Celcius,Reamur;
        boolean valid=false;
        Scanner input = new Scanner(System.in);
        System.out.println("----------------------------------------------------");
        System.out.println("| SELAMAT DATANG DI PROGRAM KONVERSI SUHU SEDERHANA | ");
        System.out.println("-----------------------------------------------------");
        System.out.print(k);
        String ans = input.next();
        while(ans.equals("y") || ans.equals("Y"))
        {
            System.out.print("\n");
            System.out.println("Pilih Jenis Suhu Saat Ini Untuk di Konversi");
            System.out.println("1. Celcius");
            System.out.println("2. Fahrenheit");
            System.out.println("3. Kelvin");
            System.out.println("4. Reamur");
            System.out.println("Masukkan Pilihan : ");
            int suhu = input.nextInt();
            System.out.print("Masukkan nilai suhu awal :");
            SuhuAwal=input.nextDouble();
            switch(suhu){
                case 1:
                    Reamur = fromCelcius.toReamur();
                    Fahrenheit = fromCelcius.toFahrenheit();
                    Kelvin = fromCelcius.toKelvin();
                    System.out.println("Suhu awal ="+SuhuAwal+" Celcius\nHasil:");
                    System.out.println("Fahrenheit : "+Fahrenheit);
                    System.out.println("Kelvin : "+Kelvin);
                    System.out.println("Reamur : "+Reamur);
                    valid=true;
                    break;
                case 2:
                    Celcius = fromFahrenheit.toCelcius();
                    Reamur = fromFahrenheit.toReamur();
                    Kelvin = fromFahrenheit.toKelvin();
                    System.out.println("Suhu awal ="+SuhuAwal+" Fahrenheit\nHasil:");
                    System.out.println("Celcius : "+Celcius);
                    System.out.println("Kelvin : "+Kelvin);
                    System.out.println("Reamur : "+Reamur);
                    valid=true;
                    break;
                case 3:
                    Celcius = fromKelvin.toCelcius();
                    Reamur = fromKelvin.toReamur();
                    Fahrenheit = fromKelvin.toFahrenheit();
                    System.out.println("Suhu awal ="+SuhuAwal+" Kelvin\nHasil:");
                    System.out.println("Celcius : "+Celcius);
                    System.out.println("Fahrenheit : "+Fahrenheit);
                    System.out.println("Reamur : "+Reamur);
                    valid=true;
                    break;
                case 4:
                    Celcius = fromReamur.toCelcius();
                    Fahrenheit = fromReamur.toFahrenheit();
                    Kelvin = fromReamur.toKelvin();
                    System.out.println("Suhu awal ="+SuhuAwal+" Reamur\nHasil:");
                    System.out.println("Celcius : "+Celcius);
                    System.out.println("Fahrenheit : "+Fahrenheit);
                    System.out.println("Kelvin : "+Kelvin);
                    valid=true;
                    break;
                default :
                    System.out.println("Pilih Jenis Suhu Secara Benar!!!");
                    break;
            }
            System.out.print("\n");
            System.out.print("Ingin Lanjut "+k);
            ans = input.next();
        } if(ans.equals("n") || ans.equals("N")){
            System.out.println("program selesai");
        }
    }
}
